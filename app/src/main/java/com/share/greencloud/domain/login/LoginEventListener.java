package com.share.greencloud.domain.login;

public interface LoginEventListener {
    public void onLogin(LoginType loginType);
}
