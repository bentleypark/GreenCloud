package com.share.greencloud.domain.login;

public enum LoginType {
    FACEBOOK,
    KAKAO,
    GOOGLE,
    NAVER
}
